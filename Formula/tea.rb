# Brew formula to build and install gitea tea from source tarball
#
# brew audit --strict --git --online tea
#
class Tea < Formula
  desc "Command-line tool to interact with Gitea servers"
  homepage "https://gitea.com/gitea/tea"
  license "MIT"

  stable do
    url "https://gitea.com/gitea/tea/archive/v0.7.0.tar.gz"
    sha256 "0d65b49410321535ffcbba7795651cbef81911552f12f7617c9cf77a9d7d5da9"
  end

  head do
    url "https://gitea.com/gitea/tea.git", branch: "master"
  end

  depends_on "go" => :build

  def install
    system "make", "build"

    bin.install "#{buildpath}/tea" => "tea"
  end

  test do
    system "#{bin}/tea", "--version"
  end
end
