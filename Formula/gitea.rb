# Brew formula to build and install gitea from source tarball
#
# brew audit --strict --git --online --formula gitea
#
class Gitea < Formula
  desc "Self-hosted git service"
  homepage "https://github.com/go-gitea/gitea"
  license "MIT"

  stable do
    version "1.14.3"
    url "https://github.com/go-gitea/gitea/releases/download/v#{version}/gitea-src-#{version}.tar.gz"
    sha256 `curl -sL #{url}.sha256`.split.first
  end

  head do
    url "https://github.com/go-gitea/gitea.git", branch: "main"
  end

  option "without-sqlite", "Do not build with sqlite as database"

  depends_on "go" => :build
  depends_on "node" => :build

  def install
    ENV.deparallelize
    ENV.append_path "PATH", "#{Formula["node"].libexec}/bin" # {npm,npx}

    tags = "bindata"
    tags << " sqlite sqlite_unlock_notify" if build.with? "sqlite"
    ENV["TAGS"] = tags

    system "make", "build"

    bin.install "#{buildpath}/gitea" => "gitea"
  end

  test do
    system "#{bin}/gitea", "--version"
  end
end
