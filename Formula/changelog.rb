# Brew formula to build and install gitea changelog from source tarball
#
# brew audit --strict --git --online changelog
#
class Changelog < Formula
  desc "Generate changelog of gitea repository"
  homepage "https://gitea.com/gitea/changelog"
  license "MIT"

  stable do
    url "https://gitea.com/gitea/changelog/archive/v0.2.0.tar.gz"
    sha256 "9c181dc6b15583ced061ded574aee6b2040ff8e3f1f7de999afe88b6698b74d2"
  end

  head do
    url "https://gitea.com/gitea/changelog.git", branch: "master"
  end

  depends_on "go" => :build

  def install
    system "make", "build"

    bin.install "#{buildpath}/changelog" => "changelog"
  end

  test do
    system "#{bin}/changelog", "--version"
  end
end
