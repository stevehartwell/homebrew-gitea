# Brew cask to install gitea from binary distribution
#
# brew audit --strict --git --online --cask gitea
#
cask "gitea" do
  module Utils
    def self.gitea_disturl(product, version)
      root_url = "https://github.com/go-gitea/gitea/releases/download/v#{version}"
      os = OS.mac? ? "darwin-10.12" : "linux"
      arch = { i386: "386", x86_64: "amd64", arm64: "arm64" }[Hardware::CPU.arch]
      raise "Gitea: Unsupported system architecture #{Hardware::CPU.arch}" if arch.nil?
      "#{root_url}/#{product}-#{version}-#{os}-#{arch}"
    end
  end

  version "1.14.3"
  sha256 `curl -sL #{Utils.gitea_disturl("gitea", version)}.sha256`.split.first

  url Utils.gitea_disturl("gitea", version)
  name "Gitea"
  desc "Self-hosted git web service"
  homepage "https://github.com/go-gitea/gitea"

  container type: :naked

  binary File.basename(url.path), target: "gitea"
end
